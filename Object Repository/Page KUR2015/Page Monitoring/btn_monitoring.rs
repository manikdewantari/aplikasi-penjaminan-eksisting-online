<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_monitoring</name>
   <tag></tag>
   <elementGuidId>29151f7d-57ce-4cf7-91c9-dcc5bbbffee7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(.,'MONITORING')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(.,'MONITORING')]</value>
      <webElementGuid>b7fb373b-59d8-4557-a2f5-63e495926c10</webElementGuid>
   </webElementProperties>
</WebElementEntity>
