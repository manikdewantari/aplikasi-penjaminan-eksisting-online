<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_TOB</name>
   <tag></tag>
   <elementGuidId>d7146651-74f6-41ce-a1e0-184bbd357f07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(.,'TRANSFER OF BRANCH')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(.,'TRANSFER OF BRANCH')]</value>
      <webElementGuid>c34f69d6-cf00-4b27-8b26-a0440e0c952e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
