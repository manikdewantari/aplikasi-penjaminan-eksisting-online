<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>subtn_TOB</name>
   <tag></tag>
   <elementGuidId>8b1d6dc0-17ef-46d7-bf73-83e31809d464</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(.,'Transfer of Branch (TOB)')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(.,'Transfer of Branch (TOB)')]</value>
      <webElementGuid>fb44c7bc-97da-479b-a7ea-7852647fb85a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
