<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_bankcabang</name>
   <tag></tag>
   <elementGuidId>2c1b68a3-5618-4687-bb33-8f594c5dcf6d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(.,'Bank Cabang')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(.,'Bank Cabang')]</value>
      <webElementGuid>46457094-2b04-4850-bf84-24bf18536e0d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
