<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_bank</name>
   <tag></tag>
   <elementGuidId>1dc390b0-9e49-411b-9da3-807318fdec2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@submenu_bank = concat(&quot;//a[contains(.,&quot; , &quot;'&quot; , &quot;BANK&quot; , &quot;'&quot; , &quot;)]&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>submenu_bank</name>
      <type>Main</type>
      <value>//a[contains(.,'BANK')]</value>
      <webElementGuid>e5392beb-c892-495b-84e3-5ab7b97ca8d1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
